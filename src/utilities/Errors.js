class Errors {
    /**
     * Create a new Errors instance.
     */
    constructor() {
        this.errors = {};
        this.field = '';
    }

    /**
     * Validate an input field
     * the validateAgainst param string can contain
     * multiple rules for example if you wanted to
     * say a field is required and contains a valid email
     * you would pass the string: required|validEmail
     *
     * @param {string} validateAgainst
     * @param {string} field
     * @param {string} fieldValue
     * @return {boolean}
     */
    validateInput(validateAgainst, field, fieldValue) {

      this.clear();
      if(typeof validateAgainst !== 'undefined'){
        var validateMethods = validateAgainst.split('|');
          for (var i = 0; i < validateMethods.length; i++) {
              // call methods
              this.field = field;
              // refactor and remove eval(); !!
              return eval('this.'+validateMethods[i]+ '(fieldValue)');
          }
        }
    }

    /**
     * Validate on form submit
     * WIP method - not finished yet!
     *
     * @param {string} validateAgainst
     * @param {string} field
     */
    validateSubmit(validateAgainst, field){
      this.validateInput(validateAgainst, field, null);
    }

    /**
     * Validate and required input and
     * check that a value exists
     *
     * @param {string} value
     * @return {boolean}
     */
    required(value){

      if(value === ''){
        this.record('This field is mandatory');
        return false;
      }

      return true;

    }

    /**
     * Validate the input against a
     * valid email address
     *
     * @param {string} value
     */
    validEmail(value){

      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if(re.test(value) === false){

        this.record('Please provide a valid email address');
        return false;

      }

      return true;

    }

    /**
     * Determine if an errors exists for the given field.
     *
     * @param {string} field
     * @return {string}
     */
    has() {
        return this.errors;
    }


    /**
     * Determine if we have any errors.
     */
    any() {
        return Object.keys(this.errors).length > 0;
    }


    /**
     * Retrieve the error message for a field.
     *
     * @param {string} field
     */
    get() {
        if (Object.keys(this.errors).length != 0) {
            return this.errors[this.field];
        }
    }


    /**
     * Record the new errors.
     *
     * @param {object} errors
     */
    record(errors) {
        this.errors[this.field] = errors;
    }


    /**
     * Clear one or all error fields.
     *
     * @param {string|null} field
     */
    clear() {
        this.errors = {};
    }
}

export default Errors;
